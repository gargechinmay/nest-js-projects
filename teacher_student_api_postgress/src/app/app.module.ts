import { Module } from '@nestjs/common';
import { StudentsModule } from 'src/students/students.module';
import { TeachersModule } from 'src/teachers/teachers.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from 'src/connection/typeorm.connection';

@Module({
  imports: [ StudentsModule , TeachersModule   , TypeOrmModule.forRoot( typeOrmConfig ) ] , 
})
export class AppModule {}
  