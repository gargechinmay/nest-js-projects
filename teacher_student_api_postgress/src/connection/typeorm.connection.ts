import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { Students } from "src/students/students.entity";
import { Teachers } from "src/teachers/teachers.entity";

export const typeOrmConfig : TypeOrmModuleOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'Chinmay',
    database: 'stu_teach_api',
    entities: [Teachers , Students],
    synchronize: true, // should be false on production
    autoLoadEntities : true
  }
