import { Module } from '@nestjs/common';
import { ProjectService } from './project.service';
import { ProjectResolver } from './project.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './entities/project.entity';
import { Employee } from 'src/employee/entities/employee.entity';

@Module({
  imports : [ TypeOrmModule.forFeature([Project , Employee]) ] ,
  providers: [ProjectResolver, ProjectService] , 
  exports : [ ProjectService ]
})
export class ProjectModule {}
