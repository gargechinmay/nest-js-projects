import { Resolver, Query, Mutation, Args, Int, ResolveField, Parent } from '@nestjs/graphql';
import { EmployeeService } from './employee.service';
import { Employee } from './entities/employee.entity';
import { CreateEmployeeInput } from './dto/create-employee.input';
import { UpdateEmployeeInput } from './dto/update-employee.input';
import { Project } from 'src/project/entities/project.entity';

@Resolver(() => Employee)
export class EmployeeResolver {
  constructor(private readonly employeeService: EmployeeService) {}

  @Query( () => [Employee] , { name : "getAllEmployee" } )
  findAll(){
    return this.employeeService.findAll()
  }
 
  @Mutation( () => Employee , { name : "createEmployee" } )
  createEmployee( @Args('employee') employee : CreateEmployeeInput ) {
    return this.employeeService.createEmployee(employee)
  }

  @ResolveField( () => {
    return Project;
  } )
  project( @Parent() employee : Employee ){
    return this.employeeService.getProject(employee.projectId  )
  }
}
