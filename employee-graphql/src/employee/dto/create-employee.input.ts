import { InputType, Int, Field } from '@nestjs/graphql';
import { Column } from 'typeorm';

@InputType()
export class CreateEmployeeInput {

  @Field()
  @Column()
  f_name : string

  @Field()
  @Column()
  l_name : string

  @Field()
  @Column()
  designation : string

  @Field()
  @Column()
  city: string

  @Field()
  projectId : string
}
