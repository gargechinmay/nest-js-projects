import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { async } from 'rxjs';
import { Project } from 'src/project/entities/project.entity';
import { ProjectService } from 'src/project/project.service';
import { Repository } from 'typeorm';
import { CreateEmployeeInput } from './dto/create-employee.input';
import { UpdateEmployeeInput } from './dto/update-employee.input';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeeService {

  constructor(
    @InjectRepository(Employee)
     private employeeRepo : Repository<Employee> ,
    private projectService : ProjectService
  ){}

  findAll() : Promise<Employee[]>{
    return this.employeeRepo.find()
  }

   createEmployee( employee : CreateEmployeeInput ) : Promise<Employee> {
     return this.employeeRepo.save(employee)
  }

  async getProject ( id : string ) : Promise<Project> {
    return this.projectService.findOne(id)
  }
}
