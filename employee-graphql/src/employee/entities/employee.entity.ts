import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Project } from 'src/project/entities/project.entity';
import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';

@ObjectType()
@Entity()
export class Employee {

  @Field()
  @PrimaryGeneratedColumn('uuid')
  id : string

  @Field()
  @Column()
  f_name : string

  @Field()
  @Column()
  l_name : string

  @Field()
  @Column()
  designation : string

  @Field()
  @Column()
  city: string

  @ManyToOne(() => Project , project => project.employees  )
  @Field(() => Project )
  @JoinColumn()
  project : Project

  @Column()
  @Field()
  projectId  : string
}
