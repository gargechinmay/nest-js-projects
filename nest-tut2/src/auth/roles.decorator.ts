import { SetMetadata } from '@nestjs/common';
import { Roles } from 'src/user/user.interface';

export const Roles_Key = 'roles'

export const RoleDeco = (...roles: Roles[]) => SetMetadata( Roles_Key , roles );
