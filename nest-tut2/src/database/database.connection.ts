import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { Cat } from "src/cat/cat.entity";

export const typeOrmConfig : TypeOrmModuleOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'Chinmay',
    entities : [ Cat ] ,
    database: 'nest-tut-2',
    synchronize: true, // should be false on production
    autoLoadEntities : true
  }
