import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable, tap } from 'rxjs';
import * as chacheManager from "cache-manager"
import { Reflector } from '@nestjs/core';


@Injectable()
export class ChacheManagerInterceptor implements NestInterceptor  {

  constructor (
    private readonly reflector : Reflector
  ) {}
  manager = chacheManager.caching( { 
    store : 'memory' , 
    max : 100 , 
    ttl : 10
   } )
   async intercept(context: ExecutionContext, next: CallHandler): Promise<any> {
     const keys = this.reflector.get("cache-key" , context.getHandler() )
    const cached = await this.manager.get( keys )
    if ( cached ) {
      return cached
    }
    return next.handle().pipe(
       tap( response => {
        this.manager.set( keys , response )
      } ) 
    )
  }
}
