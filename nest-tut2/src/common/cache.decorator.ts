import { SetMetadata } from '@nestjs/common';

export const CacheKey = (...args: string[]) => SetMetadata('cache-key', args);
