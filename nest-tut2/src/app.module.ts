import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatModule } from './cat/cat.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './database/database.connection';
import { RolesGuard } from './auth/roles.guard';
import { UserModule } from './user/user.module';
import { ChacheManagerInterceptor } from './common/chache-manager.interceptor';

@Module({
  imports: [ CatModule, TypeOrmModule.forRoot(typeOrmConfig)  , RolesGuard , UserModule  ,ChacheManagerInterceptor ],
  controllers: [AppController],
  providers: [AppService  ],
  exports : [  ]
})
export class AppModule {}
