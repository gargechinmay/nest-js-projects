import { Catch, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import { IUser, IUserCred } from './user.interface';
import * as bcrypt from 'bcrypt';
import { InvalidCred, NotFoundException } from 'src/common/exceptionfilter';
import { JwtService } from '@nestjs/jwt';
import { config } from 'process';

@Injectable()
export class UserService {

    constructor( 
        @InjectRepository(UserEntity)
        private readonly userRepo : Repository<UserEntity> , 
        private readonly JwtService : JwtService
    ) {}

     async addUser ( user : IUser ) : Promise<IUser> {
        user.password = await bcrypt.hash( user.password , 12 )
        return this.userRepo.save(user)
    }

    getUser() : Promise<IUser[]> {
        return this.userRepo.find()
    }

    getByUsername( username : string ) : Promise<IUser> {
        return this.userRepo.findOneOrFail( {
            where : {
                username : username
            }
        } )
    }

    async loginUser( user : IUserCred )  {
        try {
            const userFound = await this.getByUsername( user.username )
            const isSame = await bcrypt.compare( user.password , userFound.password )
            if ( !isSame ) {
                throw new InvalidCred()
            } 
            const { user_id , ...payload } = userFound
            const token =  this.JwtService.sign(  payload )
            console.log(  userFound , isSame , token  )
            return {
                token : token 
            }
        }
        catch (e) {
           
            throw new InvalidCred()
        }
     
    }
}
