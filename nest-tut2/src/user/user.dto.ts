import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsOptional, IsString } from "class-validator";
import { Roles } from "./user.interface";


export class UserDto {
    @IsOptional()
    readonly user_id! : number 

    @ApiProperty({
        description: "username of user (unique)* " , 
        required : true
    })
    @IsString()
    readonly username : string

    @ApiProperty({
        description: " password of user " , 
        required : true
    })
    @IsString()
    readonly password : string

    @IsEnum( Roles )
    @ApiProperty( {
        description : " Role of User [  Admin , EDITOR , SUBEDITOR , ] "
    } )
    readonly role : Roles
}

export class LoginDto {

    @ApiProperty({
        description: "username of user " , 
        required : true
    })
    @IsString()
    readonly username : string

    @ApiProperty({
        description: " password of user " , 
        required : true
    })
    @IsString()
    readonly password : string


}