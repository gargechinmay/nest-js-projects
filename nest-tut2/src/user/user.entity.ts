import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Roles } from "./user.interface";




@Entity("users")
export class UserEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    user_id : number 

    @Column ( { type : 'varchar' , unique : true  })
    username : string 

    @Column ( { type : "varchar"  } )
    password : string

    @Column ( { type : 'enum' , enum : Roles , default : Roles.Admin })
    role : Roles
}