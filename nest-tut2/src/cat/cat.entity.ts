import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity('cat')

export class Cat extends BaseEntity { 

    @PrimaryGeneratedColumn()
    cat_id : number 

    @Column( { type : "varchar" })
    cat_name : string

    @Column( { type : "int" } )
    cat_age : number

    @Column( { type : "varchar" })
    cat_breed : string


}