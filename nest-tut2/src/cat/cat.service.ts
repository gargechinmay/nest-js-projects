import { HttpException, Injectable } from '@nestjs/common';
import { ICat } from './cat.interface';
import { v4 } from "uuid"
import { createCatDto } from './cat.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Cat } from './cat.entity';
import { NotFoundException } from 'src/common/exceptionfilter';

@Injectable()
export class CatService {

    private readonly cats: ICat[] = []

    constructor(
        @InjectRepository(Cat)
        private readonly catRepo: Repository<Cat>
    ) { }

    create(cat: ICat): Promise<any> {
        return this.catRepo.save(cat)
    }

    findAll(): Promise<ICat[]> {
        return this.catRepo.find()
    }

    async getById(id: number): Promise<ICat> {
        const cat = await this.catRepo.findOne(id)
        if (!cat) {
            throw new NotFoundException()
        }
        return cat
    }
}
