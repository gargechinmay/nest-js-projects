import { Controller, Get, Param, ParseUUIDPipe,  } from '@nestjs/common';
import { Teachers } from './teachers.entity';
import { TeachersService } from './teachers.service';

@Controller('teachers')
export class TeacherController {

    constructor(private readonly teacherService: TeachersService){}

    @Get()
    async getTeachers(): Promise<Teachers[]> {
        return this.teacherService.getTeachers()
    } 

 }