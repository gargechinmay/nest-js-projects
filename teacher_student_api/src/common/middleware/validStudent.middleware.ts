import { HttpException, Injectable , NestMiddleware } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Request , Response , NextFunction } from "express";
import { Students } from "src/students/students.entity";
import { Repository } from "typeorm";


@Injectable() // needed to mark it as middleware / services 
export class ValidStudentMiddleware implements NestMiddleware {

    constructor ( 
        @InjectRepository(Students)
        private StudentRepo : Repository<Students> ,
    ){}


    async use ( req : Request  , res : Response , next : NextFunction ) {

        const studentId = req.params.studentId  
        const stuExits = this.StudentRepo.findOneOrFail(studentId)
        try {
            await stuExits
            next()
        }
        catch {
            throw new HttpException ( "Student Not Found " , 400 )
        }
    } // middleware always needs to have use method
}