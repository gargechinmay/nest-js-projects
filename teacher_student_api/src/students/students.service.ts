import { Inject, Injectable } from '@nestjs/common';
import {  CreateStudentDto, UpdateStudentDto } from './dto/students.dto';
import { Students } from './students.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Teachers } from 'src/teachers/teachers.entity';

@Injectable() // provider
export class StudentsService {

    constructor ( 
        @InjectRepository(Students)
        private StudentRepo : Repository<Students> ,
    ){}

    async getStudents() : Promise<Students[]> {
        return this.StudentRepo.find()
    }
    
    async getStudentById( studentId : number ) : Promise<Students> {
        try {   
            const stu = this.StudentRepo.findOneOrFail( {
                student_id : studentId
            } )
            return stu
        }
        catch  (err){
            throw err 
        }
    }

    async updateStudent ( studentId : number ,  data : UpdateStudentDto ) : Promise<Students> {
        const stu = await this.getStudentById(studentId)
        stu.student_name = data.student_name
        stu.marks = data.marks
        return this.StudentRepo.save(stu)
}
   async getParticularStudent ( teacherId : number ) : Promise<Students[]> {
        try {
            const students = this.StudentRepo.find(  {
                where : {
                    teacher_id : teacherId
                }
            } ) 
            return students
         
        }
        catch ( err ) {
            throw err
        }
    }

   async createStudent ( teacherId : number , data : CreateStudentDto ) : Promise<Students> {

        const newStudent = this.StudentRepo.create(
            {
               student_name : data.student_name ,
               marks : data.marks ,
               teacher_id : teacherId
            }
        )        

        return this.StudentRepo.save(newStudent)

        
    }
}
