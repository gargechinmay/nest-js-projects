import { type } from "os";
import { Teachers } from "src/teachers/teachers.entity";
import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity("students") // table name

export class Students extends BaseEntity {
    @PrimaryGeneratedColumn()
    student_id : number

    @Column({type : "varchar"})
    student_name : string 

    @Column( { type : "int" , nullable : true }  )
    marks : number
   
    @Column({ nullable :true } )
    teacher_id : number
    @ManyToOne( () => Teachers , entity => entity.teacher_id )
    @JoinColumn({ 
        name : "teacher_id"
    })
    teacher : Teachers
  
    
}


